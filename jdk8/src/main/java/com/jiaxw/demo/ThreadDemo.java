package com.jiaxw.demo;

/**
 * 多线程
 *
 * @author jiaxw
 * @date 2020/11/8 12:41
 */
public class ThreadDemo {

    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("匿名内部类 开启新线程");
            }
        }).start();

        System.out.println("========== 分割线 ==========");

        new Thread(() -> {
            System.out.println("Lambda 表达式 开启新线程");
        }).start();

    }
}