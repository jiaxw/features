package com.jiaxw.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 集合操作
 *
 * @author jiaxw
 * @date 2020/11/8 18:05
 */
public class StreamDemo {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        // 集合添加数据
        Collections.addAll(list, "张三", "李四", "王五", "赵六");

        // forEach
        list.stream().forEach(System.out::println);

        System.out.println("========== 分割线 ==========");

        // count
        System.out.println(list.stream().count());

        System.out.println("========== 分割线 ==========");

        // limit
        list.stream().limit(3).forEach(System.out::println);

        System.out.println("========== 分割线 ==========");

        // skip
        list.stream().skip(3).forEach(System.out::println);

        System.out.println("========== 分割线 ==========");

        // map
        list.stream().map(s -> s + "1").forEach(System.out::println);

        System.out.println("========== 分割线 ==========");

        List<Integer> list2 = new ArrayList<>();
        // 集合添加数据
        Collections.addAll(list2, 2, 3, 6, 5, 5, 3);

        // sorted 升序
        list2.stream().sorted().forEach(System.out::println);

        System.out.println("========== 分割线 ==========");

        // sorted 倒序
        list2.stream().sorted((a, b) -> b - a).forEach(System.out::println);

        System.out.println("========== 分割线 ==========");

        // distinct
        list2.stream().distinct().forEach(System.out::println);

        System.out.println("========== 分割线 ==========");

    }

}